package hello.upload.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@Data
public class UploadFile {

    private String uploadFileName;
    private String storeFileName;
}