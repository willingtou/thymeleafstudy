package hello.itemservice.domain.item.Entity.useGroups;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 이전에 Item 객체에 대해 엔티티에서 검증 했던것들 기록을 그대로 남겨둠.
 */
//@ScriptAssert(
//        lang="javascript",script = "_this.price * this.quantity >= 10000",
//        message = "총 합이 만원 넘어야 합니다."//제약조건이 너무많아서 사용x
//)
@Data
public class ValidatedItem {

//    //수정 요구사항 추가
//    @NotNull
    @NotNull(groups = UpdateCheck.class) //수정시에만 적용
    private Long id;

    @NotBlank(groups = {SaveCheck.class, UpdateCheck.class})
    private String itemName;

    @NotNull(groups = {SaveCheck.class, UpdateCheck.class})
    @Range(min = 1000, max = 1000000,
            groups = {SaveCheck.class, UpdateCheck.class})
    private Integer price;

    @NotNull(groups = {SaveCheck.class, UpdateCheck.class})
    @Max(value = 9999, groups = SaveCheck.class) //등록시에만 적용
    private Integer quantity;

    public ValidatedItem() {
    }

    public ValidatedItem(String itemName, Integer price, Integer quantity) {
        this.itemName = itemName;
        this.price = price;
        this.quantity = quantity;
    }
}
