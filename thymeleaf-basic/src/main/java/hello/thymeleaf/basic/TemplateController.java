package hello.thymeleaf.basic;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/template")
public class TemplateController {

    //footer 템플릿조각 예시
    @GetMapping("/fragment")
    public String template() {
        return "template/fragment/fragmentMain";
    }

    //코드조각을 레이아웃에 전달 예시 (head,link 전달)
    @GetMapping("/layout")
    public String layout() {
        return "template/layout/layoutMain";
    }

    //템플릿 레이아웃 확장 (html 전체 전달)
    @GetMapping("/layoutExtend")
    public String layoutExtends() {
        return "template/layoutExtend/layoutExtendMain";
    }


}
